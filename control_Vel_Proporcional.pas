//
// 

// Global Variables
var
  Vnom: double;

procedure Control;
var Pulses: integer;
    Volts, Gain: double;
begin
  Pulses := GetAxisOdo(0, 0);
  SetRCValue(2,2,format('%d',[Pulses]));

  // Manual Control
  Volts := GetRCValue(1, 2);
  SetAxisVoltageRef(0, 0, Volts);
end;

procedure Initialize;
begin
  SetMotorActive(0, 0, true);
end;
