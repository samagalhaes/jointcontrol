// Global Variables
var
  Integral: double;
  Ti,Tc, Kc: double;

procedure Control;
var Pulses: integer;
    Volts, Reference, Error: double;

begin
  // Leitura
  Pulses := GetAxisOdo(0, 0);
  Reference := GetRCValue (3,2);
  
    // Processo
  Error := Reference - Pulses;
  Integral := Integral + Error*Tc;
  Volts := Kc *(Error + (1/Ti)*Integral);
  
  // Anti-Windup
  if (Volts > 12) then begin
    Volts := 12;
    Integral := Integral - Error*Tc;
  end;
  if (Volts < -12) then begin
    Volts := -12;
    Integral := Integral - Error*Tc;
  end;
  
  // Escrita
  SetAxisVoltageRef(0, 0, Volts);
  SetRCValue(2,2,format('%d',[Pulses]));
  SetRCValue (1,2,format('%g',[Volts]));
  SetRCValue (5,2, format('%g',[Error]))
end;

procedure Initialize;
begin
  SetMotorActive(0, 0, true);

  Integral := 0;
  Ti := 0.159;
  Tc := 0.05
  Kc := 0.303;
end;
