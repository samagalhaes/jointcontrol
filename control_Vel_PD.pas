// Global Variables
var
  Integral_out, Error_ant: double;
  Ti,Tc, Kc, Td: double;

procedure Control;
var Pulses: integer;
    Volts, Reference, Error, Derivative, Pulses_second: double;

begin
  // Leitura
  Pulses := GetAxisOdo(0, 0);
  Reference := GetRCValue (3,2);
  
    // Controlador
  Pulses_second := Pulses / Tc;
  Integral_out := Integral_out + Pulses_second*Tc;
  Error := Reference - Integral_out;
  Derivative := (Error-Error_ant)/Tc;
  Volts := Kc *(Error + Td*Derivative);
  
  Error_ant := Error;
  
  if (Volts < 0.6) and (Volts > 0) then
    Volts := 1;

  if (Volts > -0.6) and (Volts < 0) then
    Volts := -1;
    
    // Anti-Windup
  if (Volts > 12) then begin
    Volts := 12;
  end;
  if (Volts < -12) then begin
    Volts := -12;
  end;

  
  // Escrita
  SetAxisVoltageRef(0, 0, Volts);
  
  SetRCValue(2,2,format('%d',[Pulses]));
  SetRCValue (1,2,format('%g',[Volts]));
  SetRCValue (5,2, format('%g',[Error]));
  SetRCValue (7,2, format('%g',[Integral_out]));
end;

procedure Initialize;
begin
  SetMotorActive(0, 0, true);

  Integral_out := 0;
  Error_ant := 0;
  
  Ti := 0.159;
  Tc := 0.05;
  Td := 0.1005;
  Kc := 0.0409;
end;
